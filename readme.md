Python and network exercise
=============================

To use

1. [Fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) the project
2. `git clone` to your workstation
3. change stuff, do your own tests, `git commit`, ie. do programming.
4. run `./test.sh`
5. If it fails, read the messages an go back to 3.
    If it suceeds, continue below
6. `git push` to gitlab
7. The CI part on gitlab will run the tests also. If it fails, go to 3. and fix it.
6. Team up with someone else
7. One starts the server, `python3 server.py`
8. The other changes the client program to use the ip of the other.
9. Start the client, `python3 client.py`
10. 9/11 inside job

How can you make the tests pass, even when you must be able to connect to other IPs?
